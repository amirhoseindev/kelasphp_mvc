<div class="header-container  ">
    <header id="st_header" class="animated fast">
        <section id="mobile_bar" class="animated fast">
            <div class="container">
                <div id="mobile_bar_top" class="flex_container">
                    <div id="mobile_bar_left">
                        <div class="flex_container">
                            <a class="mobile_logo" href="https://www.salambaba.com/" title="سلام بابا">
                                <img class="logo" src="public/site/img/سلام-بابا-logo-1592593371.jpg" alt="سلام بابا" width="100" height="28">
                            </a>

                            <a id="rightbar_9" href="javascript:;" class="mobile_bar_tri  menu_mobile_bar_tri mobile_bar_item  " data-name="side_stmobilemenu" data-direction="open_bar_left" rel="nofollow" title="فهرست">
                                <i class="fto-menu-1 fs_xl"></i>
                                <span class="mobile_bar_tri_text">فهرست</span>
                            </a>

                        </div>
                    </div>
                    <div id="mobile_bar_center" class="flex_child">
                        <div class="flex_container ">
                        </div>
                    </div>
                    <div id="mobile_bar_right">
                        <div class="flex_container">
                            <a id="rightbar_6" rel="nofollow" title="مشاهده سبد خرید من" href="javascript:;" class="mobile_bar_tri  cart_mobile_bar_tri mobile_bar_item shopping_cart_style_2" data-name="side_products_cart" data-direction="open_bar_right">
                                <div class="ajax_cart_bag">
                                    <span class="ajax_cart_quantity amount_circle ">0</span>
                                    <span class="ajax_cart_bg_handle"></span>
                                    <i class="fto-glyph fs_xl"></i>
                                </div>
                                <span class="mobile_bar_tri_text">سبد خرید</span>
                            </a>
                            <a id="rightbar_12" data-name="side_custom_sidebar_12" data-direction="open_bar_right" href="https://salambaba.com/login?back=my-account" class="  custom_mobile_bar_tri mobile_bar_item" rel="nofollow" title="پروفایل ">
                                <i class="fto-user-o fs_xl"></i>
                                <span class="mobile_bar_tri_text">پروفایل </span>
                            </a>
                        </div>
                    </div>
                </div>
                <div id="mobile_bar_bottom" class="flex_container">
                    <div class="search_widget_block">
                        <div class="search_widget" data-search-controller-url="//www.salambaba.com/search">
                            <form method="get" action="https://www.salambaba.com/search" class="search_widget_form">
                                <div class="search_widget_form_inner input-group round_item js-parent-focus input-group-with-border">
                                    <input type="text" class="form-control search_widget_text js-child-focus" name="s" value="" placeholder="جستجو در فروشگاه‌ما" autocomplete="off">
                                    <span class="input-group-btn">
	        <button class="btn btn-search btn-less-padding btn-spin search_widget_btn link_color icon_btn" type="submit"><i class="fto-search-1"></i></button>
	      </span>
                                </div>

                            </form>
                            <div class="search_results  search_show_img  search_show_name  search_show_price ">
                                <div class="autocomplete-suggestions" style="display: none;"></div>
                            </div>
                            <a href="javascript:;" title="محصولات بیشتر." rel="nofollow" class="display_none search_more_products go">برای محصولات بیشتر کلیک کنید.</a>
                            <div class="display_none search_no_products">هیچ محصولی پیدا نشد.</div>
                        </div>
                    </div>

                </div>
            </div>
        </section>




        <div id="header_primary" class="">
            <div class="wide_container">
                <div id="header_primary_container" class="container">
                    <div id="header_primary_row" class="flex_container  logo_left ">
                        <div id="header_left" class="">
                            <div class="flex_container header_box  flex_left ">
                                <div class="logo_box">
                                    <div class="slogan_horizon">
                                        <a class="shop_logo" href="https://www.salambaba.com/" title="سلام بابا">
                                            <img class="logo" src="site/img/سلام-بابا-logo-1592593371.jpg" alt="سلام بابا" width="100" height="28">
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div id="header_center" class="">
                            <div class="flex_container header_box  flex_center ">
                            </div>
                        </div>
                        <div id="header_right" class="">
                            <div id="header_right_top" class="flex_container header_box  flex_right ">
                                <div class="search_widget_block">
                                    <div class="search_widget" data-search-controller-url="//www.salambaba.com/search">
                                        <form method="get" action="https://www.salambaba.com/search" class="search_widget_form">
                                            <div class="search_widget_form_inner input-group round_item js-parent-focus input-group-with-border">
                                                <input type="text" class="form-control search_widget_text js-child-focus" name="s" value="" placeholder="محصول مورد نظرتان را جستجو کنید ..." autocomplete="off">
                                                <span class="input-group-btn">
	        <button class="btn btn-search btn-less-padding btn-spin search_widget_btn link_color icon_btn" type="submit"><i class="fto-search-1"></i></button>
	      </span>
                                            </div>

                                        </form>
                                        <div class="search_results  search_show_img  search_show_name  search_show_price ">
                                            <div class="autocomplete-suggestions" style="display: none;"></div>
                                        </div>
                                        <a href="javascript:;" title="محصولات بیشتر." rel="nofollow" class="display_none search_more_products go">برای محصولات بیشتر کلیک کنید.</a>
                                        <div class="display_none search_no_products">هیچ محصولی پیدا نشد.</div>
                                    </div>
                                </div>
                                <div class="blockcart dropdown_wrap top_bar_item shopping_cart_style_2 clearfix" data-refresh-url="//www.salambaba.com/module/stshoppingcart/ajax">
                                    <a href="https://www.salambaba.com/cart?action=show" title="مشاهده سبد خرید من" rel="nofollow" class="st_shopping_cart dropdown_tri header_item " data-name="side_products_cart" data-direction="open_bar_right">
                                        <div class="flex_container">
                                            <div class="ajax_cart_bag mar_r4"><span class="ajax_cart_quantity amount_circle ">0</span><span class="ajax_cart_bg_handle"></span><i class="fto-glyph icon_btn"></i></div>
                                        </div>
                                    </a>
                                    <div class="dropdown_list cart_body ">
                                        <div class="dropdown_box">
                                            <div class="shoppingcart-list">
                                                <div class="cart_empty">سبد خرید شما خالی است.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div> <a class="login top_bar_item" href="https://www.salambaba.com/my-account" rel="nofollow" title="ورود به حساب کاربری"><span class="header_item"><i class="fto-user icon_btn header_v_align_m fs_big mar_r4"></i></span></a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <section id="top_extra" class="main_menu_has_widgets_0">
            <div class="sticky-wrapper" style="">
                <div class="st_mega_menu_container animated fast" style="">
                    <div class="container">
                        <div id="top_extra_container" class="flex_container ">
                            <nav id="st_mega_menu_wrap" class="">
                                <ul class="st_mega_menu clearfix mu_level_0">
                                    <li id="st_menu_2" class="ml_level_0 m_alignment_0">
                                        <a id="st_ma_2" href="https://www.salambaba.com/86-tv" class="ma_level_0 is_parent" title="تلویزیون ">تلویزیون</a>
                                        <div class="stmenu_sub style_wide col-md-12" style="display: none;">
                                            <div class="row m_column_row">
                                                <div id="st_menu_column_4" class="col-md-2">
                                                    <div id="st_menu_block_14">
                                                        <ul class="mu_level_1">
                                                            <li class="ml_level_1">
                                                                <a id="st_ma_14" href="https://www.salambaba.com/98-brand-tv" title="بر اساس مارک" class="ma_level_1 ma_item">بر اساس مارک</a>
                                                                <ul class="mu_level_2 p_granditem_1">
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/95-brand-lg" title="تلویزیون ال جی" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>تلویزیون ال جی</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/107-tv-philips" title="تلویزیون فیلیپس" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>تلویزیون فیلیپس</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/96-%D8%AA%D9%84%D9%88%DB%8C%D8%B2%DB%8C%D9%88%D9%86-%D8%B3%D8%A7%D9%85%D8%B3%D9%88%D9%86%DA%AF" title="تلویزیون سامسونگ" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>تلویزیون سامسونگ</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/113-Sharp-tv" title="تلویزیون شارپ" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>تلویزیون شارپ</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/250-Toshiba-tv" title="تلویزیون توشیبا" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>تلویزیون توشیبا</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/120-sony" title="تلویزیون سونی" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>تلویزیون سونی</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/276-Hisense" title="تلویزیون هایسنس" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>تلویزیون هایسنس</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/342-tv-xiaomi" title="تلویزیون شیائومی " class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>تلویزیون شیائومی </a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/111-Tv-panasonic" title="تلویزیون پاناسونیک" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>تلویزیون پاناسونیک</a>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div id="st_menu_column_5" class="col-md-2">
                                                    <div id="st_menu_block_15">
                                                        <ul class="mu_level_1">
                                                            <li class="ml_level_1">
                                                                <a id="st_ma_15" href="https://www.salambaba.com/149-Quality" title="بر اساس کیفیت" class="ma_level_1 ma_item">بر اساس کیفیت</a>
                                                                <ul class="mu_level_2 p_granditem_1">
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/123-%DA%A9%DB%8C%D9%81%DB%8C%D8%AA-suhd" title="کیفیت SUHD" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>کیفیت SUHD</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/127-4k" title="کیفیت 4K" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>کیفیت 4K</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/150-full-hd" title="کیفیت FULL HD" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>کیفیت FULL HD</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/151-HD-TV" title="کیفیت تصویر HD" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>کیفیت تصویر HD</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/280-%DA%A9%DB%8C%D9%81%DB%8C%D8%AA-8k" title="کیفیت 8k" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>کیفیت 8k</a>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div id="st_menu_block_21">
                                                        <ul class="mu_level_1">
                                                            <li class="ml_level_1">
                                                                <a id="st_ma_21" href="https://www.salambaba.com/109-Audio-syste" title="سیستم صوتی تلویزیون" class="ma_level_1 ma_item">سیستم صوتی تلویزیون</a>
                                                                <ul class="mu_level_2 p_granditem_1">
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/148-%D8%B3%DB%8C%D9%86%D9%85%D8%A7-%D8%AE%D8%A7%D9%86%DA%AF%DB%8C" title="سینما خانگی" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>سینما خانگی</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/160-sound-system" title="سیستم صوتی - شیک" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>سیستم صوتی - شیک</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/234-%D8%B3%D8%A7%D9%86%D8%AF%D8%A8%D8%A7%D8%B1" title=" ساندبار" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i> ساندبار</a>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div id="st_menu_column_6" class="col-md-2">
                                                    <div id="st_menu_block_16">
                                                        <ul class="mu_level_1">
                                                            <li class="ml_level_1">
                                                                <a id="st_ma_16" href="https://www.salambaba.com/97-by-size" title="بر اساس اندازه" class="ma_level_1 ma_item">بر اساس اندازه</a>
                                                                <ul class="mu_level_2 p_granditem_1">
                                                                    <li class="ml_level_2 granditem_1 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/91-32-inch" title="32 اینچ" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>32 اینچ</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_1 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/92-tv-40-43-inch" title="40-43 اینچ" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>40-43 اینچ</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_1 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/93-tv-49-50-inch" title="49-50 اینچ" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>49-50 اینچ</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_1 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/99-58-60-inch" title="58-60 اینچ" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>58-60 اینچ</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_1 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/94-55-inch" title="55 اینچ" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>55 اینچ</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_1 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/100-tv-75-inch" title="تلویزیون 75 اینچ " class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>تلویزیون 75 اینچ </a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_1 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/254-65-inch" title="65 اینچ" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>65 اینچ</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_1 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/310-82-85-%D8%A7%DB%8C%D9%86%DA%86" title="82-85 اینچ" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>82-85 اینچ</a>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div id="st_menu_column_7" class="col-md-2">
                                                    <div id="st_menu_block_17">
                                                        <ul class="mu_level_1">
                                                            <li class="ml_level_1">
                                                                <a id="st_ma_17" href="https://www.salambaba.com/142-TV" title="براساس نوع" class="ma_level_1 ma_item">براساس نوع</a>
                                                                <ul class="mu_level_2 p_granditem_1">
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/87-led" title="LED" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>LED</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/88-qled" title="QLED" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>QLED</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/89-oled" title="OLED" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>OLED</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/101-3D" title="سه بعدی" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>سه بعدی</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/112-curved" title="منحنی" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>منحنی</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/159-smart" title="اسمارت - هوشمند" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>اسمارت - هوشمند</a>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div id="st_menu_column_8" class="col-md-2">
                                                    <div id="st_menu_block_18">
                                                        <ul class="mu_level_1">
                                                            <li class="ml_level_1">
                                                                <a id="st_ma_18" href="https://www.salambaba.com/152-os" title="براساس سیستم عامل" class="ma_level_1 ma_item">براساس سیستم عامل</a>
                                                                <ul class="mu_level_2 p_granditem_1">
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/153-android" title="اندروید" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>اندروید</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/154-Tizen" title="تایزن" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>تایزن</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/155-webos" title="webOS" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>webOS</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/158-%D8%B3%DB%8C%D8%B3%D8%AA%D9%85-%D8%B9%D8%A7%D9%85%D9%84-%D9%81%D8%A7%DB%8C%D8%B1%D9%81%D8%A7%DA%A9%D8%B3-%D9%BE%D8%A7%D9%86%D8%A7%D8%B3%D9%88%D9%86%DB%8C%DA%A9" title="سیستم عامل فایرفاکس" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>سیستم عامل فایرفاکس</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/301-%D8%B3%DB%8C%D8%B3%D8%AA%D9%85-%D8%B9%D8%A7%D9%85%D9%84-%D9%84%DB%8C%D9%86%D9%88%DA%A9%D8%B3" title="سیستم عامل لینوکس" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>سیستم عامل لینوکس</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/323-vidaa" title="VIDAA" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>VIDAA</a>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div id="st_menu_column_9" class="col-md-2">
                                                    <div id="st_menu_block_19">
                                                        <ul class="mu_level_1">
                                                            <li class="ml_level_1">
                                                                <a id="st_ma_19" href="https://www.salambaba.com/118-accessories" title="لوازم جانبی" class="ma_level_1 ma_item">لوازم جانبی</a>
                                                                <ul class="mu_level_2 p_granditem_1">
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/122-dvd" title="دی وی دی " class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>دی وی دی </a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/130-tv-screen-saver" title="محافظ صفحه تلویزیون " class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>محافظ صفحه تلویزیون </a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/133-HDMI" title="اچ دی ام آی" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>اچ دی ام آی</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/134-%D8%A8%D8%B1%D8%A7%DA%A9%D8%AA-%D8%AA%D9%84%D9%88%DB%8C%D8%B2%DB%8C%D9%88%D9%86" title="براکت تلویزیون" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>براکت تلویزیون</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/135-TABLE" title="میز تلویزیون" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>میز تلویزیون</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/137-remot" title="کنترل تلویزیون" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>کنترل تلویزیون</a>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="st_menu_6" class="ml_level_0 m_alignment_0">
                                        <a id="st_ma_6" href="https://www.salambaba.com/125-big-home" class="ma_level_0 is_parent" title="خانگی بزرگ">خانگی بزرگ</a>
                                        <div class="stmenu_sub style_wide col-md-12">
                                            <div class="row m_column_row">
                                                <div id="st_menu_column_10" class="col-md-2-4">
                                                    <div id="st_menu_block_22">
                                                        <ul class="mu_level_1">
                                                            <li class="ml_level_1">
                                                                <a id="st_ma_22" href="https://www.salambaba.com/132-said-by-said" title="یخچال" class="ma_level_1 ma_item">یخچال</a>
                                                                <ul class="mu_level_2 p_granditem_1">
                                                                    <li class="ml_level_2 granditem_1 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/195-lg-side-by-side" title="یخچال ساید ال جی" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>یخچال ساید ال جی</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_1 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/196-samsung-side-by-side" title="یخچال ساید سامسونگ" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>یخچال ساید سامسونگ</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_1 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/197-HITACHI-side-by-side" title="یخچال ساید هیتاچی" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>یخچال ساید هیتاچی</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_1 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/199-Refrigerator" title="یخچال بالا و پایین" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>یخچال بالا و پایین</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_1 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/235-Twin-fridge" title="یخچال دوقلو" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>یخچال دوقلو</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_1 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/281-said-bosch" title="یخچال و ساید بوش" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>یخچال و ساید بوش</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_1 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/282-said-daewoo" title="یخچال و ساید دوو" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>یخچال و ساید دوو</a>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div id="st_menu_column_11" class="col-md-2-4">
                                                    <div id="st_menu_block_24">
                                                        <ul class="mu_level_1">
                                                            <li class="ml_level_1">
                                                                <a id="st_ma_24" href="https://www.salambaba.com/38-washing-machine" title="لباسشویی" class="ma_level_1 ma_item">لباسشویی</a>
                                                                <ul class="mu_level_2 p_granditem_1">
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/239-washing-lg" title="لباسشویی ال جی" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>لباسشویی ال جی</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/240-washing-samsung" title="لباسشویی سامسونگ" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>لباسشویی سامسونگ</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/244-washing-bosch" title="لباسشویی بوش" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>لباسشویی بوش</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/245-washing-machine-beko" title="ماشین لباسشویی بکو" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>ماشین لباسشویی بکو</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/247-%D9%84%D8%A8%D8%A7%D8%B3%D8%B4%D9%88%DB%8C%DB%8C-%D8%AF%D9%88%D9%88" title="لباسشویی دوو" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>لباسشویی دوو</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/249-hitachi-washing" title="لباسشویی هیتاچی" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>لباسشویی هیتاچی</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/290-%D9%84%D8%A8%D8%A7%D8%B3%D8%B4%D9%88%DB%8C%DB%8C-%D9%87%D8%A7%DB%8C%D8%B3%D9%86%D8%B3" title="لباسشویی هایسنس" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>لباسشویی هایسنس</a>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div id="st_menu_column_12" class="col-md-2-4">
                                                    <div id="st_menu_block_25">
                                                        <ul class="mu_level_1">
                                                            <li class="ml_level_1">
                                                                <a id="st_ma_25" href="https://www.salambaba.com/40-dishwashing-machine" title="ظرفشویی" class="ma_level_1 ma_item">ظرفشویی</a>
                                                                <ul class="mu_level_2 p_granditem_1">
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/237-dishwasher-lg" title="ظرفشویی ال جی" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>ظرفشویی ال جی</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/238-dishwasher-samsung" title="ظرفشویی سامسونگ" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>ظرفشویی سامسونگ</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/241-dishwasher-bosch" title="ظرفشویی بوش" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>ظرفشویی بوش</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/242-dishwasher-beko" title="ظرفشویی بکو" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>ظرفشویی بکو</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/243-dishwasher-daewoo" title="ظرفشویی دوو" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>ظرفشویی دوو</a>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div id="st_menu_column_23" class="col-md-4">
                                                    <div id="st_menu_block_38" class="style_content">
                                                        <p><img style="float: left;" src="site/img/5eed3521564f8.jpg" alt="" width="351" height="300"></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="st_menu_7" class="ml_level_0 m_alignment_1">
                                        <a id="st_ma_7" href="https://www.salambaba.com/59-Air-cnditioner" class="ma_level_0 is_parent" title="کولر گازی">کولر گازی</a>
                                        <div class="stmenu_sub style_wide col-md-4">
                                            <div class="row m_column_row">
                                                <div id="st_menu_column_13" class="col-md-6">
                                                    <div id="st_menu_block_26">
                                                        <ul class="mu_level_1">
                                                            <li class="ml_level_1">
                                                                <a id="st_ma_26" href="https://www.salambaba.com/164-%DA%A9%D9%88%D9%84%D8%B1-%DA%AF%D8%A7%D8%B2%DB%8C" title="بر اساس مارک" class="ma_level_1 ma_item">بر اساس مارک</a>
                                                                <ul class="mu_level_2 p_granditem_1">
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/138-general" title="کولر گازی جنرال" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>کولر گازی جنرال</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/139-ogeneral" title="کولر گازی ا جنرال" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>کولر گازی ا جنرال</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/162-%DA%A9%D9%88%D9%84%D8%B1-%DA%AF%D8%A7%D8%B2%DB%8C-%D9%BE%D8%A7%D9%86%D8%A7%D8%B3%D9%88%D9%86%DB%8C%DA%A9" title="کولر گازی پاناسونیک" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>کولر گازی پاناسونیک</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/165-LG" title="کولر گازی ال جی " class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>کولر گازی ال جی </a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/168-GREE" title="کولر گازی گری" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>کولر گازی گری</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/183-Air-cnditioner-hitachi" title="کولر گازی هیتاچی" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>کولر گازی هیتاچی</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/184-samsung" title="کولر گازی سامسونگ " class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>کولر گازی سامسونگ </a>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div id="st_menu_column_14" class="col-md-6">
                                                    <div id="st_menu_block_27">
                                                        <ul class="mu_level_1">
                                                            <li class="ml_level_1">
                                                                <a id="st_ma_27" href="https://www.salambaba.com/169-SPLIT-WAT" title="براساس توان" class="ma_level_1 ma_item">براساس توان</a>
                                                                <ul class="mu_level_2 p_granditem_1">
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/171-9000-%DA%A9%D9%88%D9%84%D8%B1-%DA%AF%D8%A7%D8%B2%DB%8C" title="9000" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>9000</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/173-12000" title="12000" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>12000</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/174-14000" title="14000" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>14000</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/176-18000" title="18000" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>18000</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/178-24000" title="24000" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>24000</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/179-30000" title="30000" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>30000</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/181-36000" title="36000" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>36000</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/192-26000" title="26000 " class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>26000 </a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/236-48000" title="48000" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>48000</a>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="st_menu_8" class="ml_level_0 m_alignment_0">
                                        <a id="st_ma_8" href="https://www.salambaba.com/3-home-appliance" class="ma_level_0 is_parent" title="لوازم خانگی">لوازم خانگی</a>
                                        <div class="stmenu_sub style_wide col-md-12">
                                            <div class="row m_column_row">
                                                <div id="st_menu_column_15" class="col-md-2">
                                                    <div id="st_menu_block_28">
                                                        <ul class="mu_level_1">
                                                            <li class="ml_level_1">
                                                                <a id="st_ma_28" href="https://www.salambaba.com/4-Cooking-appliances" title="لوازم پخت وپز" class="ma_level_1 ma_item">لوازم پخت وپز</a>
                                                                <ul class="mu_level_2 p_granditem_1">
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/5-sandwich-maker" title="ساندویچ ساز" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>ساندویچ ساز</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/6-Fryer" title="سرخ كن" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>سرخ كن</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/7-Slow-Cooker" title="آرام پز" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>آرام پز</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/8-flavor-wave" title="هواپز " class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>هواپز </a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/9-Pressure-cooker" title="زودپر برقی" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>زودپر برقی</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/10-toaster-food" title="توستر غذا" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>توستر غذا</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/11-Rice-cooker" title="پلوپز" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>پلوپز</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/12-steam-cooker" title="بخارپز" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>بخارپز</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/13-microwave" title="مایکروویو " class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>مایکروویو </a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/84-Soup-Maker" title="سوپ ساز" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>سوپ ساز</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/102-%D8%AA%D9%88%D8%B3%D8%AA%D8%B1-%D9%86%D8%A7%D9%86" title="توستر نان" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>توستر نان</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/104-Fruit-dryer" title="میوه خشک کن" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>میوه خشک کن</a>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div id="st_menu_column_16" class="col-md-2">
                                                    <div id="st_menu_block_29">
                                                        <ul class="mu_level_1">
                                                            <li class="ml_level_1">
                                                                <a id="st_ma_29" href="https://www.salambaba.com/21-Crusher-and-Food-Processor" title="خرد کن و غذاساز" class="ma_level_1 ma_item">خرد کن و غذاساز</a>
                                                                <ul class="mu_level_2 p_granditem_1">
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/22-meat-grinder" title="چرخ گوشت" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>چرخ گوشت</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/23-Crusher" title="خرد کن" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>خرد کن</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/25-Food-Processor" title="غذاساز" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>غذاساز</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/26-mixer" title="همزن" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>همزن</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/27-Masher" title="گوشت کوب" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>گوشت کوب</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/28-Coffee-Mill" title="اسیاب قهوه" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>اسیاب قهوه</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/57-Avance-Collection" title="همه کاره" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>همه کاره</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/81-hand-blender" title="مخلوط کن دستی" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>مخلوط کن دستی</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/83-Salad-Maker" title="سالاد ساز" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>سالاد ساز</a>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div id="st_menu_column_17" class="col-md-2">
                                                    <div id="st_menu_block_30">
                                                        <ul class="mu_level_1">
                                                            <li class="ml_level_1">
                                                                <a id="st_ma_30" href="https://www.salambaba.com/29-drink-maker" title="نوشیدنی ساز" class="ma_level_1 ma_item">نوشیدنی ساز</a>
                                                                <ul class="mu_level_2 p_granditem_1">
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/30-Tea-Maker" title="چای ساز" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>چای ساز</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/31-coffe-Maker" title="قهوه ساز" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>قهوه ساز</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/32-electric-Kettle" title="کتری برقی" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>کتری برقی</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/33-juicer" title="ابمیوه گیری" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>ابمیوه گیری</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/34-orange-juicer" title="اب پرتقال گیر" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>اب پرتقال گیر</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/35-Water-dispenser" title="اب سرد کن" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>اب سرد کن</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/76-blender" title="مخلوط کن" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>مخلوط کن</a>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div id="st_menu_column_18" class="col-md-2">
                                                    <div id="st_menu_block_31">
                                                        <ul class="mu_level_1">
                                                            <li class="ml_level_1">
                                                                <a id="st_ma_31" href="https://www.salambaba.com/145-washing" title="لوازم شست شو" class="ma_level_1 ma_item">لوازم شست شو</a>
                                                                <ul class="mu_level_2 p_granditem_1">
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/39-steam-cleaner" title="بخارشوی" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>بخارشوی</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/41-Vacuum-cleaner" title="جاروبرقی" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>جاروبرقی</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/42-steam-Iron" title="اتو دستی " class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>اتو دستی </a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/43-Vacuum" title="جارو شارژی" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>جارو شارژی</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/44-ironing-prosses" title="اتو پرس" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>اتو پرس</a>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div id="st_menu_block_32">
                                                        <ul class="mu_level_1">
                                                            <li class="ml_level_1">
                                                                <a id="st_ma_32" href="https://www.salambaba.com/45-Non-electricappliances" title="لوازم غیربرقی" class="ma_level_1 ma_item">لوازم غیربرقی</a>
                                                                <ul class="mu_level_2 p_granditem_1">
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/144-pot" title="سرویس قابلمه" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>سرویس قابلمه</a>
                                                                        </div>
                                                                    </li>
                                                                    <li class="ml_level_2 granditem_0 p_granditem_1">
                                                                        <div class="menu_a_wrap">
                                                                            <a href="https://www.salambaba.com/147-Non-electric" title="لوازم کوچک غیر برقی" class="ma_level_2 ma_item "><i class="fto-angle-right list_arrow"></i>لوازم کوچک غیر برقی</a>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div id="st_menu_column_19" class="col-md-4">
                                                    <div id="st_menu_block_35" class="style_content">
                                                        <p><img style="float: left;" src="site/img/Kitchen-Appliances.jpg" alt="" width="486" height="400"></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="st_menu_9" class="ml_level_0 m_alignment_1">
                                        <a id="st_ma_9" href="https://www.salambaba.com/338-camping" class="ma_level_0 is_parent" title="کوهنوردی, سفر">کوهنوردی و سفر</a>
                                        <div class="stmenu_sub style_wide col-md-5">
                                            <div class="row m_column_row">
                                                <div id="st_menu_column_20" class="col-md-4">
                                                    <div id="st_menu_block_33">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <ul class="mu_level_1">
                                                                    <li class="ml_level_1">
                                                                        <a href="https://www.salambaba.com/341-Flashlight-and-lamp" title="چراغ قوه و لامپ" class="ma_level_1 ma_item">چراغ قوه و لامپ</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <ul class="mu_level_1">
                                                                    <li class="ml_level_1">
                                                                        <a href="https://www.salambaba.com/343-Cane-climbing" title="عصا کوهنوردی" class="ma_level_1 ma_item">عصا کوهنوردی</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <ul class="mu_level_1">
                                                                    <li class="ml_level_1">
                                                                        <a href="https://www.salambaba.com/344-Headlamp" title="چراغ پیشانی" class="ma_level_1 ma_item">چراغ پیشانی</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <ul class="mu_level_1">
                                                                    <li class="ml_level_1">
                                                                        <a href="https://www.salambaba.com/345-%D8%A7%D8%A8%D8%B2%D8%A7%D8%B1-%DA%86%D9%86%D8%AF%DA%A9%D8%A7%D8%B1%D9%87" title="ابزار چندکاره " class="ma_level_1 ma_item">ابزار چندکاره </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <ul class="mu_level_1">
                                                                    <li class="ml_level_1">
                                                                        <a href="https://www.salambaba.com/346-Travel-underlay" title="زیرانداز سفری" class="ma_level_1 ma_item">زیرانداز سفری</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <ul class="mu_level_1">
                                                                    <li class="ml_level_1">
                                                                        <a href="https://www.salambaba.com/347-Lumbar-bag" title="کیف کمری" class="ma_level_1 ma_item">کیف کمری</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <ul class="mu_level_1">
                                                                    <li class="ml_level_1">
                                                                        <a href="https://www.salambaba.com/348-Mountaineering-accessories-travel" title="لوازم جانبی کوهنوردی ، سفر" class="ma_level_1 ma_item">لوازم جانبی کوهنوردی ، سفر</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <ul class="mu_level_1">
                                                                    <li class="ml_level_1">
                                                                        <a href="https://www.salambaba.com/349-Binoculars" title="دوربین شکاری" class="ma_level_1 ma_item">دوربین شکاری</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <ul class="mu_level_1">
                                                                    <li class="ml_level_1">
                                                                        <a href="https://www.salambaba.com/351-%D8%A8%D8%A7%D8%AA%D8%B1%DB%8C-%D9%88-%D8%B4%D8%A7%D8%B1%DA%98%D8%B1" title="باتری و شارژر" class="ma_level_1 ma_item">باتری و شارژر</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <ul class="mu_level_1">
                                                                    <li class="ml_level_1">
                                                                        <a href="https://www.salambaba.com/360-sport-and-entertainment" title="ورزش و سرگرمی" class="ma_level_1 ma_item">ورزش و سرگرمی</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="st_menu_column_21" class="col-md-8">
                                                    <div id="st_menu_block_34" class="style_content">
                                                        <p><img style="float: left;" src="site/img/104-1045629_hiker.jpg" alt="" width="262" height="334"></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="st_menu_10" class="ml_level_0 m_alignment_1">
                                        <a id="st_ma_10" href="https://www.salambaba.com/page/4-about-us" class="ma_level_0 is_parent" title="درباره‌ی ما ">راهنمای مشتریان</a>
                                        <ul id="st_menu_multi_level_10" class="stmenu_sub stmenu_multi_level" style="display: none;">
                                            <li class="ml_level_1"><a id="st_ma_39" href="https://www.salambaba.com/page/4-about-us" title="درباره‌ی ما " class="ma_level_1 ma_item "><i class="fto-angle-right list_arrow"></i>درباره‌ی ما </a></li>
                                            <li class="ml_level_1"><a id="st_ma_40" href="https://www.salambaba.com/page/36-etamad" title="چگونه اعتماد کنیم ؟" class="ma_level_1 ma_item "><i class="fto-angle-right list_arrow"></i>چگونه اعتماد کنیم ؟</a></li>
                                            <li class="ml_level_1"><a id="st_ma_41" href="https://www.salambaba.com/page/8-cart" title="راهنمای ثبت سفارش انلاین" class="ma_level_1 ma_item "><i class="fto-angle-right list_arrow"></i>راهنمای خرید</a></li>
                                            <li class="ml_level_1"><a id="st_ma_42" href="https://www.salambaba.com/page/51-Charity" title="کمک به خیریه" class="ma_level_1 ma_item "><i class="fto-angle-right list_arrow"></i>کمک به خیریه</a></li>
                                            <li class="ml_level_1"><a id="st_ma_43" href="https://www.salambaba.com/blog/comments-posted/?utm_source=comments-posted" title="نظرات مشتریان" target="_blank" class="ma_level_1 ma_item "><i class="fto-angle-right list_arrow"></i>نظرات مشتریان</a></li>
                                        </ul>
                                    </li>
                                    <li id="st_menu_11" class="ml_level_0 m_alignment_0">
                                        <a id="st_ma_11" href="https://www.salambaba.com/page/26-contact-us" class="ma_level_0" title="تماس با ما">تماس با ما</a>
                                    </li>
                                    <li id="st_menu_12" class="ml_level_0 m_alignment_0">
                                        <a id="st_ma_12" href="https://www.salambaba.com/blog" class="ma_level_0" title="وبلاگ" target="_blank">وبلاگ</a>
                                    </li>
                                    <li id="st_menu_13" class="ml_level_0 m_alignment_0">
                                        <a id="st_ma_13" href="https://www.salambaba.com/blog/comments-posted" class="ma_level_0" title="از زبان مشتریان" target="_blank">از زبان مشتریان</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

        </section>

    </header>
</div>
