<footer id="footer" class="footer-container">

    <section id="footer-primary">
        <div class="wide_container">
            <div class="container">
                <div class="row footer_first_level_row">
                    <div id="stacked_footer_column_1" class="col-lg-2-4">
                        <section id="multilink_6" class="stlinkgroups_links_footer  footer_block block ">
                            <div class="title_block ">
                                <div class="title_block_inner"> <i class="fto-help-circled st_custom_link_icon icon-mar-r4"></i>راهنمای مشتریان
                                </div>
                                <div class="opener"><i class="fto-plus-2 plus_sign"></i><i class="fto-minus minus_sign"></i></div>
                            </div>
                            <ul class="footer_block_content bullet custom_links_list ">
                                <li>

                                    <a href="https://www.salambaba.com/page/4-about-us" class="dropdown_list_item  stmultilink_item_32" title="درباره‌ی ما " rel="nofollow">
                                        <i class="fto-id-card-o  list_arrow  st_custom_link_icon"></i>درباره‌ی ما
                                    </a>
                                </li>
                                <li>

                                    <a href="https://www.salambaba.com/page/28-PRICE" class="dropdown_list_item  stmultilink_item_33" title="ارزان بودن کالا" rel="nofollow">
                                        ارزان بودن کالا
                                    </a>
                                </li>
                                <li>

                                    <a href="https://www.salambaba.com/page/7-faq" class="dropdown_list_item  stmultilink_item_30" title="سؤالات متداول" rel="nofollow">
                                        سؤالات متداول
                                    </a>
                                </li>
                                <li>

                                    <a href="https://www.salambaba.com/page/31-h-c" class="dropdown_list_item  stmultilink_item_31" title="راهنمای مشتریان " rel="nofollow">
                                        راهنمای مشتریان
                                    </a>
                                </li>
                                <li>

                                    <a href="https://www.salambaba.com/page/1-delivery" class="dropdown_list_item  stmultilink_item_19" title="شرایط ارسال - حمل و نقل" rel="nofollow">
                                        شرایط ارسال و حمل و نقل
                                    </a>
                                </li>
                                <li>

                                    <a href="https://www.salambaba.com/page/37-gozaresh" class="dropdown_list_item  stmultilink_item_34" title="گزارش خطا در سلام بابا" rel="nofollow">
                                        گزارش خطا در سلام بابا
                                    </a>
                                </li>
                            </ul>
                        </section>
                    </div>
                    <div id="stacked_footer_column_2" class="col-lg-2-4">
                        <section id="multilink_7" class="stlinkgroups_links_footer  footer_block block ">
                            <div class="title_block ">
                                <div class="title_block_inner"> لینک های مفید
                                </div>
                                <div class="opener"><i class="fto-plus-2 plus_sign"></i><i class="fto-minus minus_sign"></i></div>
                            </div>
                            <ul class="footer_block_content bullet custom_links_list ">
                                <li>

                                    <a href="https://www.salambaba.com/prices-drop" class="dropdown_list_item  stmultilink_item_20" title="حراجی‌ها">
                                        حراجی‌ها
                                    </a>
                                </li>
                                <li>

                                    <a href="https://www.salambaba.com/my-account" class="dropdown_list_item  stmultilink_item_23" title="مدیریت حساب کاربری من" rel="nofollow">
                                        حساب کاربری من
                                    </a>
                                </li>
                                <li>

                                    <a href="https://www.salambaba.com/new-products" class="dropdown_list_item  stmultilink_item_21" title="محصولات جدید">
                                        محصولات جدید
                                    </a>
                                </li>
                                <li>

                                    <a href="https://www.salambaba.com/stores" class="dropdown_list_item  stmultilink_item_15" title="فروشگاه‌های ما">
                                        فروشگاه‌های ما
                                    </a>
                                </li>
                                <li>

                                    <a href="https://www.salambaba.com/contact-us" class="dropdown_list_item  stmultilink_item_16" title="تماس با ما">
                                        تماس با ما
                                    </a>
                                </li>
                                <li>

                                    <a href="https://www.salambaba.com/%D9%86%D9%82%D8%B4%D9%87%20%D8%B3%D8%A7%DB%8C%D8%AA" class="dropdown_list_item  stmultilink_item_17" title="نقشه سایت">
                                        نقشه سایت
                                    </a>
                                </li>
                            </ul>
                        </section>
                    </div>
                    <div id="stacked_footer_column_3" class="col-lg-3">
                        <section id="st_news_letter_2" class="st_news_letter_2  hidden-md-down  footer_block block ">
                            <div class="title_block flex_container title_align_0">
                                <div class="flex_child title_flex_left"></div>
                                <div class="title_block_inner">خبرنامه</div>
                                <div class="flex_child title_flex_right"></div>
                                <div class="opener"><i class="fto-plus-2 plus_sign"></i><i class="fto-minus minus_sign"></i></div>
                            </div>
                            <div class="footer_block_content   text-1 text-md-2 ">
                                <div class="st_news_letter_box">
                                    <div class="alert alert-danger hidden"></div>
                                    <div class="alert alert-success hidden"></div>
                                    <div class="news_letter_0 ">
                                        <div class="st_news_letter_content style_content flex_child_md ">
                                            <p class="m-b-1">با عضویت در خبرنامه&nbsp; اولین کسی باشید که از فروش های ویژه و کدهای تخفیف باخبر می شوید</p>
                                        </div>
                                        <form action="https://www.salambaba.com/module/stnewsletter/ajax" method="post" class="st_news_letter_form flex_child">

                                            <div class="st_news_letter_form_inner">
                                                <div class="input-group round_item js-parent-focus input-group-with-border">
                                                    <input class="form-control st_news_letter_input js-child-focus" type="text" name="email" value="" placeholder="ایمیل شما">
                                                    <span class="input-group-btn">
			                	<button type="submit" name="submitStNewsletter" class="btn btn-less-padding st_news_letter_submit link_color">
				                    <i class="fto-mail-alt"></i>
				                </button>
			                </span>
                                                </div>
                                            </div>
                                            <input type="hidden" name="action" value="0">
                                            <input type="hidden" name="submitNewsletter" value="1">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div id="stacked_footer_column_4" class="col-lg-4">
                        <section id="easycontent_20" class=" easycontent  footer_block block ">
                            <div class="style_content footer_block_content  keep_open  ">
                                <!-- MODULE st easy content -->
                                <div id="steasy_column_26" class="row ">

                                    <div id="steasy_column_27" class="col-lg-12 col-sm-12 col-12 steasy_column ">
                                        <div class="steasy_element_block">
                                            <div class="row">


                                                <div id="steasy_element_33" class="col-lg-12 sttext_block sttext_1 sttext_1_1 ">
                                                    <div class="steasy_element_item text-1  text-md-2   clearfix ">

                                                        <div class="sttext_item_content ">
                                                            <h5 style="text-align: center; display: flex; justify-content: left; align-items: center;"><strong><a href="tel:09187886127">0918-7886127</a></strong> &nbsp;&nbsp;
                                                                <a href="https://t.me/salambaba" data-tipso="با تلگرام به روز باشید" target="_blank"><img src="./site/img/tg.png" alt="کانال تلگرام" width="28" height="28"></a><a href="https://api.whatsapp.com/send?phone=+989187886127" data-tipso="گفتگو با کارشناسان فروش "
                                                                                                                                                                                                                                          target="_blank">&nbsp; <img src="./site/img/whatsapp.png" alt="ارتباط در واتساپ" width="28" height="28"></a> &nbsp;&nbsp;
                                                                <a href="https://www.instagram.com/salambaba_com/" target="_blank"><img src="./site/img/instagram-sketched.png" alt="کانال اینستاگرام" width="28" height="28"></a>
                                                            </h5>
                                                            <p style="text-align: left;"></p>
                                                            <p style="text-align: left;"><span>کردستان بانه خ-شهدا-مجتمع تجاری هدیه-طبقه ۲</span></p>
                                                            <p style="text-align: left;"></p>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div id="steasy_column_28" class="col-lg-12 col-sm-12 col-12 steasy_column ">
                                        <div class="steasy_element_block">
                                            <div id="steasy_element_35" class="sttext_banner ">
                                                <div class="sttext_banner_1_1  ">
                                                    <div class="sttext_banner_text text-3  text-md-2   ">

                                                    </div>
                                                    <div class="sttext_banner_btn  text-2   text-md-2  flex_child_md">
                                                        <a href="market://details?id=com.salambaba.v3" title="دانلود اپلیکیشن سلام بابا" rel="nofollow" class="btn  btn-default  sttext_banner_first_btn ">دانلود اپلیکیشن سلام بابا</a>                                                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <!-- MODULE st easy content -->
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section id="footer-secondary">
        <div class="wide_container">
            <div class="container">
                <div class="row footer_first_level_row">
                    <section id="easycontent_16" class=" easycontent col-lg-12 footer_block block ">
                        <div class="style_content footer_block_content  keep_open  ">
                            <!-- MODULE st easy content -->
                            <div id="steasy_column_20" class="row ">

                                <div id="steasy_column_21" class="col-lg-2-4 col-sm-12 col-12 steasy_column ">
                                    <div class="steasy_element_block">
                                        <div class="row">


                                            <div id="steasy_element_27" class="col-lg-12 sttext_block sttext_1 sttext_1_1 ">
                                                <div class="steasy_element_item text-0  text-md-0   clearfix ">

                                                    <div class="sttext_item_content ">
                                                        <p style="display: flex; justify-content: center;">
                                                            <a target="_blank" href="https://www.salambaba.com/namad.html"> <img src="site/img/namad_1.png" style="cursor: pointer;" id="xfOweYptkQVgIbrLcM8H" width="115" height="125"></a>
                                                            <a target="_blank" href="https://www.salambaba.com/namad.html"> <img id="jxlzfukzjzpewlaorgvj" style="cursor: pointer;" alt="logo-samandehi" src="./site/img/saman.png" width="125" height="125"></a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div id="steasy_column_22" class="col-lg-10 col-sm-12 col-12 steasy_column ">
                                    <div class="steasy_element_block">
                                        <div class="row">


                                            <div id="steasy_element_28" class="col-lg-12 sttext_block sttext_1 sttext_1_1 ">
                                                <div class="steasy_element_item text-0  text-md-0   clearfix ">

                                                    <div class="title_block flex_container title_align_0 title_style_1 sttext_item_header">
                                                        <div class="flex_child title_flex_left"></div>
                                                        <div class="title_block_inner">فروشگاه اینترنتی سلام بابا</div>
                                                        <div class="flex_child title_flex_right"></div>
                                                    </div>
                                                    <div class="sttext_item_content ">
                                                        <p style="text-align: justify;">سلام بابا یکی از بروزترین و بزرگترین فروشگاههای اینترنتی سطح کشور ، همینک در آستانه‌ی پنجمین سال فعالیت، با گسترده‌ای از کالاهای متنوع برای تمام اقشار جامعه برای کاربران خود
                                                            «تجربه‌ی لذت‌ بخش یک خرید اینترنتی» را تداعی می‌کند. «ارسال سریع»، «ضمانت بهترین قیمت» و «تضمین اصل بودن کالا» سه اصل اولیه است که سلام بابا از نخستین روز تاسیس سعی کرده
                                                            به آن پایبند باشد . هدف سلام بابا همواره ایجـاد رونـدی همیـشگـی از تجـربـه ای عالـی برای مشتـریـان و کلیه افراد در هر زمان و مکان بوده است. فرقی نمی کند خریدار باشید یا
                                                            فروشنده ، از هر قشر و در هر سطح و هر جایی باشید ما مشتـاقانـه امیدواریم تجـارت الکتـرونیـک بخشـی از زنـدگـی روزمـره شمـا شـود و هر زمان در صدد به روز بودن هرچه بیشتر شما
                                                            قدم بر می داریم . فروشـگاه سـلام بـابـا وارد کننده یا تامین کننده کالا نیست.سـلام بـابـا با ارتباط با تامین کننده های مطمئن کالا٫سفارشات مشتریان را تهیه و ارسال میکند.به
                                                            این صورت که شما برای خرید نیاز به بررسی و شناخت تامین کننده ندارید سلام بابا به عنوان نماینده شما , شرایط خرید کالای اصلی را برایتان فراهم میکند. با سلام بابا اطمینان
                                                            خاطر و خرید کالای اصلی را تجربه کنید</p>
                                                        <p><a class="rmore rclose d-block d-sm-none">مشاهده بیشتر</a></p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!-- MODULE st easy content -->
                        </div>
                    </section>

                    <script type="text/javascript">
                        ga('send', 'pageview');
                    </script>
                </div>
            </div>
        </div>
    </section>




    <div id="footer-bottom" class="">
        <div class="wide_container">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 clearfix">
                        <aside id="footer_bottom_right">
                            <aside id="easycontent_21" class="easycontent_21   block  easycontent   ">
                                <div class="style_content  block_content ">
                                    <div class="easy_brother_block text-1 text-md-2">
                                        <p><a href="https://www.salambaba.com/page/category/2-help">راهنمای مشتریان</a>&nbsp; &nbsp; &nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.salambaba.com/page/26-contact-us" target="_blank">شماره حساب ها </a>&nbsp;&nbsp;&nbsp;
                                            |&nbsp; &nbsp;&nbsp;<a href="https://www.salambaba.com/page/3-Privacy">قوانین و مقررات</a></p>
                                    </div>
                                </div>
                            </aside>

                        </aside>
                        <aside id="footer_bottom_left">
                            2020-2015 © تمامی حقوق برای فروشگاه اینترنتی سلام بابا محفوظ است
                            <section id="multilink_10" class="stlinkgroups_links_footer_bottom ">
                                <ul class="li_fl clearfix custom_links_list">
                                </ul>
                            </section>

                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
